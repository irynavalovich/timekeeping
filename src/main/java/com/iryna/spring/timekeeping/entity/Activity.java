package com.iryna.spring.timekeeping.entity;

import com.iryna.spring.timekeeping.util.DurationConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(exclude = {"activityRequests"})
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "activities")
public class Activity {

    @Id
    @SequenceGenerator(name = "activityIdSeq",
            sequenceName = "activities_id_seq",
            allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "activityIdSeq")
    @Column(name = "id")
    private Long id;

    @Column(nullable = false)
    private String title;

    private String category;
    private Progress progress;

    @Column(length = 500)
    private String description;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Convert(converter = DurationConverter.class)
    private Duration duration;

    @ManyToMany(fetch = FetchType.EAGER,
            mappedBy = "activities",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<User> users = new HashSet<>();

    @Column(name = "users_count")
    private Integer usersCount;

    @OneToMany(mappedBy = "activity",
            cascade = CascadeType.ALL)
    private Set<ActivityRequest> activityRequests = new HashSet<>();

    public Activity(String title, String category, Progress progress, String description) {
        this.title = title;
        this.category = category;
        this.progress = progress;
        this.description = description;
    }

    public Activity(Long id, String title, String category, Progress progress, String description,
                    LocalDateTime startTime, LocalDateTime endTime, Duration duration, Set<User> users) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.progress = progress;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = duration;
        this.users = users;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() +
                "id=" + getId() +
                ", title='" + getTitle() + '\'' +
                ", category=" + getCategory() +
                ", progress=" + getProgress().getValue() +
                ", description='" + getDescription() + '\'' +
                ", startTime=" + getStartTime() +
                ", endTime=" + getEndTime() +
                ", duration=" + getDuration() +
                ", users=" + getUsers();
    }
}
