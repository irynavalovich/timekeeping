package com.iryna.spring.timekeeping.entity;

public enum RequestAction {

    JOIN("Join"),
    EXCLUDE("Exclude");

    private final String value;

    RequestAction(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

}
