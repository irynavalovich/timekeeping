package com.iryna.spring.timekeeping.service;

import com.iryna.spring.timekeeping.dto.UserUpdateDto;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.dto.UserRegistrationDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @description Interface provides business-logic methods for managing User
 */
public interface UserService extends UserDetailsService {

    /**
     * Search a user by his identifier.
     * @param id A user ID as a long value
     * @return A user object
     * @see com.iryna.spring.timekeeping.entity.User
     */
    User findUserById(Long id);

    /**
     * Create an activity
     * @param user is a data transfer object
     * @see com.iryna.spring.timekeeping.dto.UserRegistrationDto
     * @return true or false
     */
    boolean createUser(UserRegistrationDto user);

    /**
     * Update User entity
     * @param id A user ID as a long value
     * @param user a data transfer object
     */
    void updateUser(Long id, UserUpdateDto user);

    /**
     * Get all users as a page
     * @param pageable Can be a page request object
     * @return page of users
     * @see org.springframework.data.domain.Page
     * @see com.iryna.spring.timekeeping.entity.User
     */
    Page<User> getAllPageable(Pageable pageable);

    /**
     * Method can block or unblock the user
     * @param id user ID as a long value
     * @return true or false
     */
    boolean blockUnblockUser(Long id);
}
