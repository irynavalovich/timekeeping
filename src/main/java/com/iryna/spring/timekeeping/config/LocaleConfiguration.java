package com.iryna.spring.timekeeping.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * @description The class is responsible for setting the locale and Resource Bundle files.
 */

@Configuration
public class LocaleConfiguration implements WebMvcConfigurer {

    /**
     * Switch to a new locale based on the value of the lang parameter.
     *
     * @return LocaleChangeInterceptor
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {

        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;

    }

    /**
     * Resolves the locale by reading attribute from the HTTPSession for all subsequent
     * requests from one user.
     *
     * @return LocaleResolver
     */
    @Bean
    public LocaleResolver localeResolver() {

        SessionLocaleResolver resolver = new SessionLocaleResolver();
        resolver.setDefaultLocale(new Locale("be"));
        resolver.setTimeZoneAttributeName("UTC");
        return resolver;

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(localeChangeInterceptor()).addPathPatterns("/*");

    }

    /**
     * Define encoding and the path for storing message files for the supported languages.
     *
     * @return MessageSource
     */
    @Bean
    public MessageSource messageSource() {

        ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();
        source.setBasename("classpath:/messages");
        source.setDefaultEncoding("UTF-8");
        return source;

    }

    @Override
    public LocalValidatorFactoryBean getValidator() {

        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(messageSource());
        return localValidatorFactoryBean;

    }

}
