package com.iryna.spring.timekeeping.service;

import com.iryna.spring.timekeeping.entity.ActivityRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @description Interface provides business-logic methods for managing ActivityRequest entity
 */
public interface ActivityRequestService {

    /**
     * Search an activity request in the database by its identifier.
     *
     * @param id An activity request ID as a long value
     * @return ActivityRequest object
     * @see com.iryna.spring.timekeeping.entity.ActivityRequest
     */
    ActivityRequest findActivityRequestById(Long id);

    /**
     * Create activity request for joining to activity.
     * @param activityId An activity ID as a long value
     * @param userId A user ID as a long value
     * @return true or false
     */
    boolean createJoinActivityRequest(Long activityId, Long userId);

    /**
     * Create activity request for excluding from activity.
     * @param activityId An activity ID as a long value
     * @param userId A user ID as a long value
     * @return true or false
     */
    boolean createExcludeActivityRequest(Long activityId, Long userId);

    /**
     * Method for rejecting a request
     * @param id An activity request ID as a long value
     */
    void rejectActivityRequest(Long id);

    /**
     * Approve request for joining
     * @param id An activity request ID as a long value
     * @return true or false
     */
    boolean setApproveStatusForJoinActivityRequest(Long id);

    /**
     * Approve request for excluding
     * @param id An activity request ID as a long value
     * @return true or false
     */
    boolean setApproveStatusForExcludeActivityRequest(Long id);

    /**
     * Get all requests as a page
     * @param pageable Can be a page request object
     * @see org.springframework.data.domain.Pageable
     * @return page of requests
     * @see com.iryna.spring.timekeeping.entity.ActivityRequest
     * @see org.springframework.data.domain.Page
     */
    Page<ActivityRequest> getAllPageable(Pageable pageable);
}
