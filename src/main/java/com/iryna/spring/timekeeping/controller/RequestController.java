package com.iryna.spring.timekeeping.controller;

import com.iryna.spring.timekeeping.entity.ActivityRequest;
import com.iryna.spring.timekeeping.entity.RequestStatus;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.ActivityRequestNotFoundException;
import com.iryna.spring.timekeeping.service.ActivityRequestService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller for requests
 */

@Controller
@AllArgsConstructor
public class RequestController {

    private final ActivityRequestService activityRequestService;

    @GetMapping("/activities/request")
    public String getActivityRequests(Model model, @PageableDefault(sort = {"id"},
            direction = Sort.Direction.DESC) Pageable pageable){

        model.addAttribute("listActivityRequests", activityRequestService.getAllPageable(pageable));
        return "requests";

    }

    @GetMapping("/activities/request/join/{id}")
    public String createJoinActivityRequest(@AuthenticationPrincipal User user,
                                  @PathVariable(value = "id") Long activityId){

        activityRequestService.createJoinActivityRequest(activityId, user.getId());
        return "redirect:/activities";

    }

    @GetMapping("/activities/request/exclude/{id}")
    public String createExcludeActivityRequest(@AuthenticationPrincipal User user,
                                  @PathVariable(value = "id") Long activityId){

        activityRequestService.createExcludeActivityRequest(activityId, user.getId());
        return "redirect:/activities";

    }


    @GetMapping("/activities/request/reject/{id}")
    public String rejectActivityRequest(@PathVariable(value = "id") Long id) {
        ActivityRequest activityRequest = activityRequestService.findActivityRequestById(id);

        if (!activityRequest.getStatus().equals(RequestStatus.PENDING_APPROVAL)) {
            return "redirect:/activities/request";
        }

        activityRequestService.rejectActivityRequest(id);
        return "redirect:/activities/request";

    }

    @GetMapping("/activities/request/approve/{id}")
    public String setApproveStatusForActivityRequest(@PathVariable(value = "id") Long id){

        ActivityRequest ar = activityRequestService.findActivityRequestById(id);

        if (ar.getAction().getValue().equalsIgnoreCase("Join")){
            activityRequestService.setApproveStatusForJoinActivityRequest(id);
        } else {
            activityRequestService.setApproveStatusForExcludeActivityRequest(id);
        }
        return "redirect:/activities/request";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ActivityRequestNotFoundException.class)
    public String handleActivityRequestNotFoundException(ActivityRequestNotFoundException e) {

        return "error/404-status";

    }

}
