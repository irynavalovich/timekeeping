package com.iryna.spring.timekeeping.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @description Data transfer object for Activity to transport object from client to business logic
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto implements Serializable {

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 3, max = 100, message = "{validation.dto.title.size}")
    private String title;

    @NotBlank(message = "{validation.dto.not.blank}")
    private String category;

    @Size(max = 500, message = "{validation.dto.description.size}")
    private String description;

}
