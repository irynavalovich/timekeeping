package com.iryna.spring.timekeeping.entity;

public enum Progress {

    TODO("TODO"),
    ACTIVE("Active"),
    DISABLED("Disabled");

    private final String value;

    Progress(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

}
