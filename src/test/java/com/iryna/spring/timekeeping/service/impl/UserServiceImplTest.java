package com.iryna.spring.timekeeping.service.impl;

import com.iryna.spring.timekeeping.dto.UserRegistrationDto;
import com.iryna.spring.timekeeping.dto.UserUpdateDto;
import com.iryna.spring.timekeeping.entity.Role;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.UserNotFoundException;
import com.iryna.spring.timekeeping.repository.UserRepository;
import com.iryna.spring.timekeeping.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock private UserRepository userRepo;
    @Mock private PasswordEncoder passwordEncoder;
    private UserService underTest;
    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @BeforeEach
    void setUp() {

        underTest = new UserServiceImpl(userRepo, passwordEncoder);

    }

    @Test
    void canFindUserById() {

        User expected = User.builder().firstName("John")
                .lastName("Smith")
                .username("username")
                .password(passwordEncoder.encode("user.getPassword()"))
                .email("mailsm@mail.com")
                .role(Role.USER)
                .enabled(true).build();

        userRepo.save(expected);

        when(userRepo.findUserById(expected.getId()))
                .thenReturn(Optional.of(expected));

        User actual = underTest.findUserById(expected.getId());
        assertEquals(expected,actual);

        verify(userRepo, Mockito.times(1)).save(expected);
        verify(userRepo, Mockito.times(1)).findUserById(expected.getId());
        verify(passwordEncoder, Mockito.times(1)).encode("user.getPassword()");

    }

    @Test
    void throwExceptionWhenNotFindUserById() {

        Long id = 5L;

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> underTest.findUserById(id))
                .withMessage("User by id" + id + "was not found");

        verify(userRepo, Mockito.times(1)).findUserById(id);

    }

    @Test
    void canCreateUser() {

        UserRegistrationDto user = new UserRegistrationDto();
        user.setUsername("redbird");
        user.setEmail("sample@mail.com");
        user.setLastName("Samplesurname");
        user.setFirstName("Samplename");
        user.setPassword("samplepassword");

        boolean isUserCreated = underTest.createUser(user);

        assertTrue(isUserCreated);

        verify(userRepo, Mockito.times(1)).findUserByUsername("redbird");
        verify(userRepo, Mockito.times(1)).save(userArgumentCaptor.capture());

        assertThat(userArgumentCaptor.getValue()).isNotNull();
        assertThat(userArgumentCaptor.getValue().getRole()).isEqualTo(Role.USER);
        assertThat(userArgumentCaptor.getValue().isEnabled()).isEqualTo(true);
        assertThat(userArgumentCaptor.getValue().getEmail()).isEqualTo("sample@mail.com");

    }

    @Test
    void whenCreateUserFailed() {

        UserRegistrationDto user = new UserRegistrationDto();
        user.setUsername("John");

        doReturn(Optional.of(user))
                .when(userRepo)
                .findUserByUsername("John");

        boolean isUserCreated = underTest.createUser(user);

        assertFalse(isUserCreated);
        verify(userRepo, Mockito.times(0)).save(userArgumentCaptor.capture());
    }


    @Test
    void canLoadUserByUsername() {

        User expected = User.builder().firstName("John")
                .lastName("Smith")
                .username("username")
                .password(passwordEncoder.encode("user.getPassword()"))
                .email("mailsm@mail.com")
                .role(Role.USER)
                .enabled(true).build();

        userRepo.save(expected);

        when(userRepo.findUserByUsername("username"))
                .thenReturn(Optional.of(expected));

        UserDetails actual = underTest.loadUserByUsername("username");
        assertEquals(expected,actual);

        verify(userRepo, Mockito.times(1)).save(expected);
        verify(userRepo, Mockito.times(1)).findUserByUsername(expected.getUsername());
        verify(passwordEncoder, Mockito.times(1)).encode("user.getPassword()");
    }

    @Test
    void throwExceptionWhenNotLoadUsername() {

        String username = "username";

        assertThatExceptionOfType(UsernameNotFoundException.class)
                .isThrownBy(() -> underTest.loadUserByUsername(username))
                .withMessage("User by username " + username + " was not found");

        verify(userRepo, Mockito.times(1)).findUserByUsername(username);

    }

    @Test
    void whenBlockUnblockUserIsSuccessFull(){

        User u = User.builder()
                .username("username")
                .enabled(true)
                .role(Role.USER)
                .firstName("Timmy")
                .lastName("Hanks")
                .password("timmypassword")
                .id(27L)
                .build();

        userRepo.save(u);

        when(userRepo.findUserById(u.getId()))
                .thenReturn(Optional.of(u));

        assertTrue(underTest.blockUnblockUser(27L));
        assertFalse(u.isEnabled());

        u.setEnabled(false);
        assertTrue(underTest.blockUnblockUser(27L));
        assertTrue(u.isEnabled());

        verify(userRepo, times(3))
                .save(u);

    }

    @Test
    void whenBlockUnblockUserIsNotSuccessFull(){

        Long id = 50L;

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> underTest.blockUnblockUser(id))
                .withMessage("User by id" + id + "was not found");

        verify(userRepo, times(1))
                .findUserById(id);

    }

    @Test
    void whenUpdateUserIsSuccessful(){

        User u = User.builder()
                .id(27L)
                .build();

        userRepo.save(u);

        UserUpdateDto user = new UserUpdateDto();
        user.setUsername("username");
        user.setEmail("mail@mail.com");
        user.setLastName("Samplesurname");
        user.setPassword("timmypassword");
        user.setRole(Role.USER);
        user.setFirstName("Timmy");
        user.setId(27L);

        when(userRepo.findUserById(u.getId()))
                .thenReturn(Optional.of(u));

        underTest.updateUser(27L, user);
        verify(userRepo, times(2))
                .save(userArgumentCaptor.capture());
        assertThat(userArgumentCaptor.getValue()).isNotNull();
        assertThat(userArgumentCaptor.getValue().getUsername()).isEqualTo("username");
        assertThat(userArgumentCaptor.getValue().getLastName()).isEqualTo("Samplesurname");
        assertThat(userArgumentCaptor.getValue().getFirstName()).isEqualTo("Timmy");
        assertThat(userArgumentCaptor.getValue().getPassword())
                .isEqualTo(passwordEncoder.encode("timmypassword"));
        assertThat(userArgumentCaptor.getValue().getEmail()).isEqualTo("mail@mail.com");
        assertThat(userArgumentCaptor.getValue().getRole()).isEqualTo(Role.USER);

        verify(userRepo, times(2))
                .save(userArgumentCaptor.capture());

    }

    @Test
    void whenUpdateUserIsNotSuccessful() {

        Long id = 5L;

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> underTest.updateUser(id, new UserUpdateDto()))
                .withMessage("User by id" + id + "was not found");

        verify(userRepo, Mockito.times(1)).findUserById(id);
        verify(userRepo, Mockito.times(0))
                .save(userArgumentCaptor.capture());

    }
}