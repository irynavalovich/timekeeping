package com.iryna.spring.timekeeping.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ActivityDtoTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @ParameterizedTest
    @MethodSource("provideArgsForValidActivityDto")
    public void testValidActivityDto(ActivityDto activity, boolean expected) {
        Set<ConstraintViolation<ActivityDto>> violations = validator.validate(activity);

        assertEquals(expected, violations.isEmpty());

    }

    private static Stream<Arguments> provideArgsForValidActivityDto() {

        ActivityDto a1 = new ActivityDto("Title", "Group", "Some description");
        ActivityDto a2 = new ActivityDto("Simple title", "Zoom-zoom", "");
        ActivityDto a3 = new ActivityDto("Dot", "Zoom", "Just bla bla");


        return Stream.of(
                Arguments.of(a1, true),
                Arguments.of(a2, true),
                Arguments.of(a3, true)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgsForInvalidActivity")
    public void testInvalidActivityDto(ActivityDto activity, boolean expected) {
        Set<ConstraintViolation<ActivityDto>> violations = validator.validate(activity);
        assertEquals(expected, violations.isEmpty());

    }

    private static Stream<Arguments> provideArgsForInvalidActivity() {

        ActivityDto a1 = new ActivityDto("", "Group", "Some description");
        ActivityDto a2 = new ActivityDto("Si", "Zoom-zoom", "");
        ActivityDto a3 = new ActivityDto("What a piece of work is a man! " +
                "How noble in reason! How infinite in faculty! " +
                        "in form, in moving, how express and admirable!" +
                        "in action how like an angel!" +
                "in apprehension how like a god! the beauty of the world! the paragon of animals! " +
                " And yet to me, what is this quintessence of dust?", "Read", "Just bla bla");

        ActivityDto a4 = new ActivityDto("Simple", "", "");


        return Stream.of(
                Arguments.of(a1, false),
                Arguments.of(a2, false),
                Arguments.of(a3, false),
                Arguments.of(a4, false)
        );
    }
}