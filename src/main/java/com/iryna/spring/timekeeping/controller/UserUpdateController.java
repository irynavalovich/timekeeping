package com.iryna.spring.timekeeping.controller;

import com.iryna.spring.timekeeping.dto.UserUpdateDto;
import com.iryna.spring.timekeeping.entity.Role;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class UserUpdateController {

    private final UserService userService;

    @GetMapping("/users/update/{id}")
    public String getUpdate(@PathVariable(value = "id") Long id, Model model){

        User user = userService.findUserById(id);
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "update-user";

    }

    @PostMapping("/users/update/{id}")
    public String postUpdate(@PathVariable(value="id") Long id,
                             @Valid @ModelAttribute("user") UserUpdateDto user,
                             BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            return "update-user";
        }

        userService.updateUser(id, user);
        return "redirect:/users";

    }

    @ModelAttribute("user")
    public UserUpdateDto getUserUpdateDto(){

        return new UserUpdateDto();

    }
}
