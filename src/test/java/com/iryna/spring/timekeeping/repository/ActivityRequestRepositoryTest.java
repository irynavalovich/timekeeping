package com.iryna.spring.timekeeping.repository;

import com.iryna.spring.timekeeping.entity.ActivityRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestPropertySource("/application.properties")
class ActivityRequestRepositoryTest {

    @Autowired
    ActivityRequestRepository underTest;

    @Test
    public void whenFindByIdExecutedThenNullReturned() throws Exception {
        assertEquals(underTest.findActivityRequestById(7L), Optional.ofNullable(null));
    }

    @Test
    public void whenFindAllExecutedThenEmptyListReturned() {
        assertEquals(underTest.findByActivityIdAndUserId(7L, 7L), new ArrayList<>());
    }

    @Test
    void itShouldNotFindActivityById() {
        Long id = 1L;
        Optional<ActivityRequest> expected = underTest.findActivityRequestById(id);
        assertFalse(expected.isPresent());

    }

}