package com.iryna.spring.timekeeping.repository;

import com.iryna.spring.timekeeping.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository underTest;

    @AfterEach
    void tearDown(){

        underTest.deleteAll();

    }

    @Test
    void itShouldFindUserById() {

        User user = User.builder()
                .username("Katalina")
                .password("katalina")
                .enabled(true)
                .build();

        underTest.save(user);

        Optional<User> expected = underTest.findUserById(user.getId());

        assertTrue(expected.isPresent());
        assertEquals(user, expected.get());

    }

    @Test
    void itShouldNotFindUserById() {

        Long id = 1L;
        Optional<User> expected = underTest.findUserById(id);
        assertFalse(expected.isPresent());

    }

    @Test
    void itShouldFindUserByUsername() {

        User user = User.builder()
                .username("Katalina")
                .password("katalina")
                .enabled(true)
                .build();

        underTest.save(user);

        Optional<User> expected = underTest.findUserByUsername("Katalina");

        assertTrue(expected.isPresent());
        assertEquals(user, expected.get());
    }

}