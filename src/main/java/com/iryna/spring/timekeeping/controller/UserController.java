package com.iryna.spring.timekeeping.controller;

import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.UserNotFoundException;
import com.iryna.spring.timekeeping.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for users
 */

@Controller
@AllArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping("/users/block/{id}")
    public String blockUser(@PathVariable(value = "id") Long id){

        userService.blockUnblockUser(id);
        return "redirect:/users";

    }

    @RequestMapping("/users")
    public String getUsers(Model model, @PageableDefault(size = 15, sort = {"id"}) Pageable pageable){

        model.addAttribute("listUsers", userService.getAllPageable(pageable));
        return "users";

    }

    @GetMapping("/account")
    public String getAccountPage(@AuthenticationPrincipal User user, Model model) {

        model.addAttribute("user", userService.findUserById(user.getId()));
        return "user-account";

    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserNotFoundException.class)
    public String handleUserNotFoundException(UserNotFoundException e) {

        return "error/404-status";

    }

}
