package com.iryna.spring.timekeeping.repository;

import com.iryna.spring.timekeeping.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *  JPA Repository provides access to Activity entity mapping in database.
 */
@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    /**
     * Search an activity in the database by its identifier.
     *
     * @param id An activity ID as a long value
     * @return Optional activity
     */
    Optional<Activity> findActivityById(Long id);

    /*
      List<Activity> findActivityByCategoryIgnoreCase(String category);
     */

}
