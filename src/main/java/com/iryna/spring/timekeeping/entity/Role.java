package com.iryna.spring.timekeeping.entity;

public enum Role {

    USER,
    ADMIN

}
