package com.iryna.spring.timekeeping.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Duration;

/**
 * @description Convertion of Duration into integer representation and vice versa correctly
 */

@Converter
public class DurationConverter implements AttributeConverter<Duration, Long> {

    @Override
    public Long convertToDatabaseColumn(Duration attribute) {

        return (attribute == null) ? null : attribute.toMinutes();

    }

    @Override
    public Duration convertToEntityAttribute(Long dbData) {

        return (dbData == null) ? null : Duration.ofMinutes(dbData);

    }
}
