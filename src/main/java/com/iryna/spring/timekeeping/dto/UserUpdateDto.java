package com.iryna.spring.timekeeping.dto;

import com.iryna.spring.timekeeping.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @description Data access object for User to transport object from UpdateUserController to business logic
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateDto implements Serializable {

    private Long id;

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 2, max = 100, message = "{user.registration.dto.firstname}")
    private String firstName;

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 2, max = 100, message = "{user.registration.dto.firstname}")
    private String lastName;

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 5, max = 40, message = "{user.registration.dto.username}")
    private String username;

    private String password;

    @Email(message = "{user.registration.dto.email}")
    @NotBlank(message = "{validation.dto.not.blank}")
    private String email;

    private Role role;

}
