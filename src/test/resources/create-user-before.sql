DELETE FROM users;

INSERT INTO users (id, email, enabled, first_name, last_name, password, role, username) VALUES
    (1, 'tomcat@mail.com', true, 'Tommy', 'Cat', '$2a$10$BDwymTZBDFj8fvHD4ne5kel05I4a5R4iNng5GJkBHYhz5gcmKcw6K', 'USER', 'username'),
    (2, 'billy@mail.com', true, 'Bill', 'Kill', '$2a$10$Uhy/EjYQ3vC/aUNwsLKmD.Jasc/dDutOmlJ3nKybHiLXgi9N4OIuC', 'ADMIN', 'admin');


