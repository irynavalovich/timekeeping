package com.iryna.spring.timekeeping.service;

import com.iryna.spring.timekeeping.dto.DurationDto;
import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.dto.ActivityDto;
import com.iryna.spring.timekeeping.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @description Interface provides business-logic methods for managing Activity
 */
public interface ActivityService {

    /**
     * Create an activity
     * @param activityDTO is a data transfer object
     * @see com.iryna.spring.timekeeping.dto.ActivityDto
     */
    void createActivity(ActivityDto activityDTO);

    /**
     * Marks the time that the user spends on activity
     * @param id ID as a long value
     * @param user The user who is an Activity
     * @param durationDto A data transfer object
     * @see com.iryna.spring.timekeeping.dto.DurationDto
     */
    void noteDuration(Long id, User user, DurationDto durationDto);

    /**
     * Get all activities as a page
     * @param pageNo a number of page as an integer value
     * @param pageSize a size of page as an integer value
     * @param sortField a String value that is used for sorting
     * @param sortDir a String value that defines direction of sorting.
     *                This parameter can have only two variants: <strong>asc</strong> and <strong>desc</strong>
     * @return page of activities
     * @see org.springframework.data.domain.Page
     * @see com.iryna.spring.timekeeping.entity.Activity
     */
    Page<Activity> findPaginated(int pageNo, int pageSize, String sortField, String sortDir);

    /**
     * Get all activities as a page
     * @param pageable Can be a page request object
     * @return page of activities
     * @see org.springframework.data.domain.Page
     * @see com.iryna.spring.timekeeping.entity.Activity
     */
    Page<Activity> findAll(Pageable pageable);

    /**
     * Make activity disabled for users
     * @param id activity ID as a long value
     * @return true or false
     */
    boolean blockActivity(Long id);

    /**
     * Method for removing activity
     * @param id activity ID as a long value
     * @return true or false
     */
    boolean deleteActivity(Long id);

    //List<Activity> findActivityByCategoryIgnoreCase(String category);

}
