package com.iryna.spring.timekeeping.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @description Class provides ways of conversion LocalDateTime-fields into Timestamp and vice versa correctly
 */

@Converter(autoApply = true)
public class TimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {

        return (attribute == null) ? null : Timestamp.valueOf(attribute);

    }

    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp dbData) {

        return (dbData == null) ? null : dbData.toLocalDateTime();

    }
}
