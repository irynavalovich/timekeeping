
--DROP TABLE IF EXISTS activities;
CREATE TABLE IF NOT EXISTS activities
(
    id BIGINT NOT NULL,
    category VARCHAR(255),
    description VARCHAR(500),
    duration BIGINT,
    end_time TIMESTAMP,
    progress VARCHAR(255),
    start_time TIMESTAMP,
    title VARCHAR(100) NOT NULL,
    users_count INTEGER,
    CONSTRAINT activities_pkey PRIMARY KEY (id)
);

-- DROP SEQUENCE IF EXISTS activities_id_seq;
CREATE SEQUENCE IF NOT EXISTS activities_id_seq
    START 1
    INCREMENT 1;

-- DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users
(
    id BIGINT NOT NULL,
    email VARCHAR(255),
    enabled boolean NOT NULL,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) ,
    username VARCHAR(40) UNIQUE NOT NULL,
    add CONSTRAINT users_pkey PRIMARY KEY (id)
);

-- DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE IF NOT EXISTS users_id_seq
    START 1
    INCREMENT 1;

CREATE TABLE users_activities (
  user_id BIGINT NOT NULL REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  activity_id BIGINT NOT NULL REFERENCES activities (id) ON UPDATE CASCADE,
  CONSTRAINT users_activities_pkey PRIMARY KEY (user_id, activity_id)
);

-- DROP TABLE IF EXISTS activity_requests;
CREATE TABLE IF NOT EXISTS activity_requests
(
    id BIGINT NOT NULL,
    action VARCHAR(255),
    status VARCHAR(255),
    activity_id BIGINT,
    user_id BIGINT,
    add CONSTRAINTS
        activity_requests_pkey PRIMARY KEY (id),
        act_id_foreign_key FOREIGN KEY (activity_id) REFERENCES activities (id) ON UPDATE CASCADE,
        user_id_foreign_key FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE CASCADE;
);

-- DROP SEQUENCE IF EXISTS activity_requests_id_seq;
CREATE SEQUENCE IF NOT EXISTS activity_requests_id_seq
    START 1
    INCREMENT 1;


