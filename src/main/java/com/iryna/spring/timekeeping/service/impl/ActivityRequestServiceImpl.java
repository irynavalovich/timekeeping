package com.iryna.spring.timekeeping.service.impl;

import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.entity.ActivityRequest;
import com.iryna.spring.timekeeping.entity.Progress;
import com.iryna.spring.timekeeping.entity.RequestAction;
import com.iryna.spring.timekeeping.entity.RequestStatus;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.ActivityRequestNotFoundException;
import com.iryna.spring.timekeeping.exception.UserNotFoundException;
import com.iryna.spring.timekeeping.repository.ActivityRepository;
import com.iryna.spring.timekeeping.repository.ActivityRequestRepository;
import com.iryna.spring.timekeeping.repository.UserRepository;
import com.iryna.spring.timekeeping.service.ActivityRequestService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Log4j2
@AllArgsConstructor
public class ActivityRequestServiceImpl implements ActivityRequestService{

    private final ActivityRequestRepository activityRequestRepository;
    private final UserRepository userRepository;
    private final ActivityRepository activityRepository;

    @Override
    public ActivityRequest findActivityRequestById(Long id) {

        return activityRequestRepository.findActivityRequestById(id).orElseThrow(
                () -> new ActivityRequestNotFoundException("Request by id" + id + "was not found")
        );

    }

    @Override
    public Page<ActivityRequest> getAllPageable(Pageable pageable) {

        return this.activityRequestRepository.findAll(pageable);

    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean createJoinActivityRequest(Long activityId, Long userId) {

        User user = userRepository.findUserById(userId).orElseThrow(
                () -> new UserNotFoundException("User by id" + userId + "was not found"));

        Activity activity = activityRepository.findActivityById(activityId).orElseThrow(
                () -> new IllegalArgumentException("Activity by id" + activityId + "was not found")
        );

        long counter = activityRequestRepository
                .findByActivityIdAndUserId(activityId, userId)
                .stream()
                .filter(r -> r.getStatus().equals(RequestStatus.PENDING_APPROVAL) &&
                        r.getAction().equals(RequestAction.JOIN))
                .count();

        if (counter > 0){
            log.info("User has already sent request");
            return false;
        }

        if (activity.getProgress().equals(Progress.DISABLED)){
            return false;
        }
        if (activity.getProgress().equals(Progress.ACTIVE)) {
            if (activity.getUsers().contains(user)){
                return false;
            } else {
                ActivityRequest activityRequest = helpCreateJoinRequest(activity, user);
                activityRequestRepository.save(activityRequest);
                return true;
            }
        }

        ActivityRequest activityRequest = helpCreateJoinRequest(activity, user);
        log.info("Request was created");
        activityRequestRepository.save(activityRequest);
        return true;

    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean createExcludeActivityRequest(Long activityId, Long userId) {

        User user = userRepository.findUserById(userId).orElseThrow(
                () -> new UserNotFoundException("User by id" + userId + "was not found"));

        Activity activity = activityRepository.findActivityById(activityId).orElseThrow(
                () -> new IllegalArgumentException("Activity by id" + activityId + "was not found")
        );

        long counter = activityRequestRepository
                .findByActivityIdAndUserId(activityId, userId)
                .stream()
                .filter(r -> r.getStatus().equals(RequestStatus.PENDING_APPROVAL) &&
                        r.getAction().equals(RequestAction.EXCLUDE))
                .count();

        if (counter > 0) {
            log.info("User has already sent request");
            return false;
        }

        if (activity.getProgress().equals(Progress.DISABLED)){
            log.info("Activity is disabled");
            return false;
        }

        if (activity.getProgress().equals(Progress.ACTIVE)){
            if (activity.getUsers().contains(user)){
                ActivityRequest activityRequest = ActivityRequest.builder()
                        .activity(activity)
                        .user(user)
                        .action(RequestAction.EXCLUDE)
                        .status(RequestStatus.PENDING_APPROVAL)
                        .build();
                log.info("Request was created");
                activityRequestRepository.save(activityRequest);
                return true;
            }
        }

        log.info("Request was not created");
        return false;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public void rejectActivityRequest(Long id) {

        ActivityRequest activityRequest = activityRequestRepository.findActivityRequestById(id).orElseThrow(
                () -> new ActivityRequestNotFoundException("Request by id" + id + "was not found")
        );

        activityRequest.setStatus(RequestStatus.REJECTED);
        log.info("Request was rejected");
        activityRequestRepository.save(activityRequest);

    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean setApproveStatusForJoinActivityRequest(Long id) {

        ActivityRequest activityRequest = activityRequestRepository.findActivityRequestById(id).orElseThrow(
                () -> new ActivityRequestNotFoundException("Request by id" + id + "was not found")
        );

        Activity activity = activityRequest.getActivity();
        User user = activityRequest.getUser();

        if (activity.getProgress().equals(Progress.TODO)){

            activity.setProgress(Progress.ACTIVE);
            activity.setUsersCount(activity.getUsersCount() + 1);
            activity.setStartTime(LocalDateTime.now());

            user.getActivities().add(activity);

            activityRequest.setStatus(RequestStatus.APPROVED);
            activityRepository.save(activity);
            activityRequestRepository.save(activityRequest);

            log.info("Request was approved");
            return true;

        } else if (activity.getProgress().equals(Progress.DISABLED)) {

            activityRequest.setStatus(RequestStatus.REJECTED);
            activityRequestRepository.save(activityRequest);

            log.info("Request was rejected");
            return false;

        } else {

            activity.setUsersCount(activity.getUsersCount() + 1);
            user.getActivities().add(activity);
            activityRequest.setStatus(RequestStatus.APPROVED);

            activityRepository.save(activity);
            activityRequestRepository.save(activityRequest);

            log.info("Request was approved");
            return true;
        }

    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean setApproveStatusForExcludeActivityRequest(Long id) {

        ActivityRequest activityRequest = activityRequestRepository.findActivityRequestById(id).orElseThrow(
                () -> new ActivityRequestNotFoundException("Request by id" + id + "was not found")
        );

        Activity activity = activityRequest.getActivity();

        if (activity.getProgress().equals(Progress.TODO)) {
            return false;
        } else if (activity.getProgress().equals(Progress.DISABLED)) {

            activityRequest.setStatus(RequestStatus.REJECTED);
            log.info("Request was Rejected");
            activityRequestRepository.save(activityRequest);
            return false;

        } else {

            activity.setProgress(Progress.DISABLED);
            log.info("Activity got Disabled stage");

            if (activity.getUsersCount() > 0){
                activity.setUsersCount(activity.getUsersCount() - 1);
                log.info("User was excluded from counter");
            } else {
                activity.setUsersCount(activity.getUsersCount());
            }

            activity.setEndTime(LocalDateTime.now());
            activityRequest.setStatus(RequestStatus.APPROVED);
            log.info("Request was approved");

            activityRepository.save(activity);
            activityRequestRepository.save(activityRequest);
            return true;

        }

    }

    private ActivityRequest helpCreateJoinRequest(Activity activity, User user){

        return ActivityRequest.builder()
                .activity(activity)
                .user(user)
                .action(RequestAction.JOIN)
                .status(RequestStatus.PENDING_APPROVAL)
                .build();

    }
}
