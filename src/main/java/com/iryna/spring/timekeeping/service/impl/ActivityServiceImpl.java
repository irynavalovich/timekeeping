package com.iryna.spring.timekeeping.service.impl;

import com.iryna.spring.timekeeping.dto.DurationDto;
import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.entity.Progress;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.ActivityNotFoundException;
import com.iryna.spring.timekeeping.repository.ActivityRepository;
import com.iryna.spring.timekeeping.service.ActivityService;
import com.iryna.spring.timekeeping.dto.ActivityDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;

@Service
@Log4j2
public class ActivityServiceImpl implements ActivityService {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository) {

        this.activityRepository = activityRepository;

    }

    @Override
    public void createActivity(ActivityDto activityDTO) {

        Activity newAct = new Activity();
        newAct.setTitle(activityDTO.getTitle());
        newAct.setDescription(activityDTO.getDescription());
        newAct.setCategory(activityDTO.getCategory());
        newAct.setProgress(Progress.TODO);
        newAct.setUsersCount(0);
        newAct.setDuration(Duration.ZERO);

        log.info("Activity was created");
        activityRepository.save(newAct);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    @Override
    public void noteDuration(Long id, User user, DurationDto durationDto) {

        Activity activity = activityRepository.findActivityById(id).orElseThrow(
                () -> new ActivityNotFoundException("Activity by id" + id + "was not found")
        );

        if(activity.getUsers().contains(user) && activity.getProgress().equals(Progress.ACTIVE)){
            Duration duration = activity.getDuration();
            duration = duration.plusDays(durationDto.getDays())
                    .plusHours(durationDto.getHours())
                    .plusMinutes(durationDto.getMinutes());

            activity.setDuration(duration);
            log.info("Setting duration");
            activityRepository.save(activity);
        }
    }

    @Override
    public Page<Activity> findPaginated(int pageNo, int pageSize, String sortField, String sortDir) {

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();

        return activityRepository.findAll(PageRequest.of(pageNo - 1, pageSize, sort));

    }

    @Override
    public boolean blockActivity(Long id) {

        Activity activity = activityRepository.findActivityById(id).orElseThrow(
                () -> new ActivityNotFoundException("Activity by id" + id + "was not found")
        );

        if (activity.getProgress().equals(Progress.TODO)){
            activity.setProgress(Progress.DISABLED);
            log.info("Activity with id" + id + " get status Disabled");
            activityRepository.save(activity);
            return true;

        }

        return false;

    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean deleteActivity(Long id) {
        Activity activity = activityRepository.findActivityById(id).orElseThrow(
                () -> new ActivityNotFoundException("Activity by id" + id + "was not found")
        );
        if (activity.getProgress().equals(Progress.TODO)) {
            activityRepository.delete(activity);
            return true;
        }
        return false;
    }

    @Override
    public Page<Activity> findAll(Pageable pageable) {

        return activityRepository.findAll(pageable);

    }
/*
    @Override
    public List<Activity> findActivityByCategoryIgnoreCase(String category) {
        if(category != null{
            return activityRepository.findActivityByCategoryIgnoreCase(String category);
        }
        return activityRepository.findAll();

    }
*/

}

