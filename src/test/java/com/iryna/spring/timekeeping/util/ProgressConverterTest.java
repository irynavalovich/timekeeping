package com.iryna.spring.timekeeping.util;

import com.iryna.spring.timekeeping.entity.Progress;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProgressConverterTest {

    private ProgressConverter converter = new ProgressConverter();

    @Test
    void returnNullIfAttributeIsNullToDatabaseColumn() {

        assertNull(converter.convertToDatabaseColumn(null));
    }

    @Test
    void returnStringIfAttributeIsEnumToDatabaseColumn() {
        Progress progress = Progress.TODO;
        assertEquals("TODO", converter.convertToDatabaseColumn(progress));
    }

    @ParameterizedTest
    @MethodSource("provideStringsForThrowException")
    void throwExceptionIfStringIsEmptyOrHasWrongValueWhenConvertDbStringIntoEntity(
            String input,
            IllegalArgumentException e) {

        assertThrows(e.getClass(), () -> converter.convertToEntityAttribute(input));
    }

    private static Stream<Arguments> provideStringsForThrowException() {
        return Stream.of(
                Arguments.of("null", new IllegalArgumentException()),
                Arguments.of("", new IllegalArgumentException()),
                Arguments.of(" ", new IllegalArgumentException()),
                Arguments.of("  ", new IllegalArgumentException()),
                Arguments.of("bbb", new IllegalArgumentException())

        );
    }

    @Test
    void returnNullIfStringIsNullWhenConvertDbStringIntoEntity() {

        assertNull(converter.convertToEntityAttribute(null));
    }

    @ParameterizedTest
    @MethodSource("provideStringsForIsEnum")
    void returnEntityIfStringIsEnumToDatabaseColumn(String input, Progress progress) {

        assertEquals(progress, converter.convertToEntityAttribute(input));
    }

    private static Stream<Arguments> provideStringsForIsEnum() {
        return Stream.of(
                Arguments.of("TODO", Progress.TODO),
                Arguments.of("Active", Progress.ACTIVE),
                Arguments.of("Disabled", Progress.DISABLED)
        );
    }
}