package com.iryna.spring.timekeeping.service.impl;

import com.iryna.spring.timekeeping.dto.ActivityDto;
import com.iryna.spring.timekeeping.dto.DurationDto;
import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.entity.Progress;
import com.iryna.spring.timekeeping.entity.Role;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.ActivityNotFoundException;
import com.iryna.spring.timekeeping.repository.ActivityRepository;
import com.iryna.spring.timekeeping.service.ActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ActivityServiceImplTest {

    @Mock
    private ActivityRepository activityRepo;

    private ActivityService underTest;
    @Captor
    ArgumentCaptor<Activity> activityArgumentCaptor;

    @BeforeEach
    void setUp() {

        underTest = new ActivityServiceImpl(activityRepo);

    }

    @Test
    void canCreateActivity() {

        ActivityDto activityDto = new ActivityDto();
        activityDto.setTitle("Sample");
        activityDto.setCategory("CONTENT");
        activityDto.setDescription("Some description");

        underTest.createActivity(activityDto);

        verify(activityRepo, times(1))
                .save(activityArgumentCaptor.capture());

        assertThat(activityArgumentCaptor.getValue()).isNotNull();
        assertThat(activityArgumentCaptor.getValue().getDuration()).isEqualTo(Duration.ZERO);
        assertThat(activityArgumentCaptor.getValue().getProgress()).isEqualTo(Progress.TODO);
        assertThat(activityArgumentCaptor.getValue().getCategory()).isEqualTo("CONTENT");

    }


    @Test
    void noteDurationIsSuccesful() {

        User user = User.builder().username("Sample").password("samplepassword").enabled(true)
                .role(Role.USER).firstName("Samplename").lastName("Samplesurname").email("sample@mail.com")
                .id(123L)
                .build();

        Set<User> users = new HashSet<>();
        users.add(user);

        Activity activity = new Activity(124L, "SampleTitle", "CONTENT", Progress.ACTIVE, "Desc",
                LocalDateTime.now(), null, Duration.ZERO, users);

        DurationDto durationDto = new DurationDto(0, 0, 10);

        when(activityRepo.findActivityById(124L)).thenReturn(Optional.of(activity));

        underTest.noteDuration(124L, user, durationDto);

        verify(activityRepo, times(1)).save(activityArgumentCaptor.capture());

        assertThat(activityArgumentCaptor.getValue().getDuration().toMinutes()).isEqualTo(10);
    }

    @Test
    void throwExceptionWhenDurationIsNotSuccesful(){

        User user = User.builder().username("Sample").password("samplepassword").enabled(true)
                .role(Role.USER).firstName("Samplename").lastName("Samplesurname").email("sample@mail.com")
                .id(123L)
                .build();
        DurationDto durationDto = new DurationDto(0, 0, 10);

        Long id = 5L;

        assertThatExceptionOfType(ActivityNotFoundException.class)
                .isThrownBy(() -> underTest.noteDuration(id, user, durationDto))
                .withMessage("Activity by id" + id + "was not found");

        verify(activityRepo, times(0)).save(activityArgumentCaptor.capture());
        verify(activityRepo, times(1)).findActivityById(id);

    }

    @Test
    void whenBlockActivityIsSuccessFull(){

        Activity a = new Activity("Title", "Category", Progress.TODO, "Description");
        a.setId(27L);
        activityRepo.save(a);

        when(activityRepo.findActivityById(a.getId()))
                .thenReturn(Optional.of(a));

        assertTrue(underTest.blockActivity(27L));
        assertEquals(Progress.DISABLED, a.getProgress());

        verify(activityRepo, times(2))
                .save(a);

    }

    @Test
    void whenBlockActivityIsNotSuccessFull(){

        Activity a = new Activity("Title", "Category", Progress.ACTIVE, "Description");
        a.setId(27L);
        activityRepo.save(a);

        when(activityRepo.findActivityById(a.getId()))
                .thenReturn(Optional.of(a));

        assertFalse(underTest.blockActivity(27L));
        assertEquals(Progress.ACTIVE, a.getProgress());

        verify(activityRepo, times(1))
                .save(a);

    }

    @Test
    void throwExceptionBlockActivityIsNotSuccessFull(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityNotFoundException.class)
                .isThrownBy(() -> underTest.blockActivity(id))
                .withMessage("Activity by id" + id + "was not found");

        verify(activityRepo, times(1)).findActivityById(id);

    }

    @Test
    void whenDeleteActivityIsSuccessFull(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityNotFoundException.class)
                .isThrownBy(() -> underTest.deleteActivity(id))
                .withMessage("Activity by id" + id + "was not found");

        verify(activityRepo, times(1)).findActivityById(id);

    }

    @Test
    void whenDeleteActivityIsSuccessful(){

        Activity a = new Activity("Title", "Category", Progress.TODO, "Description");
        a.setId(27L);
        activityRepo.save(a);

        when(activityRepo.findActivityById(a.getId()))
                .thenReturn(Optional.of(a));

        assertTrue(underTest.deleteActivity(27L));

        verify(activityRepo, times(1))
                .delete(a);
    }

    @Test
    void whenDeleteActivityThrowException(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityNotFoundException.class)
                .isThrownBy(() -> underTest.deleteActivity(id))
                .withMessage("Activity by id" + id + "was not found");

        verify(activityRepo, times(1)).findActivityById(id);

    }

    @Test
    void whenDeleteActivityIsNotSuccessful(){

        Activity a = new Activity("Title", "Category", Progress.ACTIVE, "Description");
        a.setId(27L);
        activityRepo.save(a);

        when(activityRepo.findActivityById(a.getId()))
                .thenReturn(Optional.of(a));

        assertFalse(underTest.deleteActivity(27L));

        verify(activityRepo, times(1))
                .save(a);
        verify(activityRepo, times(0))
                .delete(a);
    }
}