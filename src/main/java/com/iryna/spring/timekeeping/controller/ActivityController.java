package com.iryna.spring.timekeeping.controller;

import com.iryna.spring.timekeeping.dto.ActivityDto;
import com.iryna.spring.timekeeping.dto.DurationDto;
import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.ActivityNotFoundException;
import com.iryna.spring.timekeeping.service.ActivityService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for activities
 */

@Controller
@AllArgsConstructor
public class ActivityController {

    private final ActivityService activityService;

    // create model attribute to bind form data
    @GetMapping("/activities/form")
    public String newActivityForm(Model model){
        Activity activity = new Activity();
        model.addAttribute("activity", activity);
        return "new-activity";
    }

    //save activity into database
    @PostMapping("/activities/add")
    public String saveActivity(@ModelAttribute("activity") @Valid ActivityDto activityDto, BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            return "new-activity";
        }

        activityService.createActivity(activityDto);
        return "redirect:/activities";

    }

    @GetMapping("activities")
    public String getAll(Model model) {

        return findPaginated(model, 1, "title", "asc");

    }

    @GetMapping("activities/{pageNo}")
    String findPaginated(Model model,
                         @PathVariable("pageNo") int pageNo,
                         @Param("sortField") String sortField,
                         @Param("sortDir") String sortDir) {

        int pageSize = 5;

        Page<Activity> page = activityService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<Activity> pageActivities = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("pageActivities", pageActivities);
        return "activities";
    }

    @GetMapping("/activities/block/{id}")
    public String blockActivity(@PathVariable(value = "id") Long id){

        activityService.blockActivity(id);

        return "redirect:/activities";

    }

    @PostMapping("/activities/note-duration/{id}")
    public String noteDuration(@AuthenticationPrincipal User user,
                               @PathVariable("id") Long id,
                               @ModelAttribute("duration") @Valid DurationDto durationDto,
                               BindingResult bindingResult,
                               @PageableDefault(size = 5, sort = {"title", "category"},
                                       direction = Sort.Direction.DESC) Pageable pageable,
                               Model model){

        if (bindingResult.hasErrors()) {
            model.addAttribute("pageActivities",
                    activityService.findAll(pageable));
            return "activities";
        }

        activityService.noteDuration(id, user, durationDto);
        return "redirect:/activities";
    }

    @GetMapping("/activities/delete/{id}")
    public String deleteActivity(@PathVariable("id") Long id) {
        activityService.deleteActivity(id);
        return "redirect:/activities";
    }


    //html page registration will take activity from Dto object according to these methods
    @ModelAttribute("activity")
    public ActivityDto getActivityDto(){

        return new ActivityDto();

    }

    @ModelAttribute("duration")
    public DurationDto getDurationDto(){

        return new DurationDto();

    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ActivityNotFoundException.class)
    public String handleActivityNotFoundException(ActivityNotFoundException e) {

        return "error/404-status";

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public String handleIllegalArgumentException(IllegalArgumentException e) {

        return "error/500-status";

    }
}
