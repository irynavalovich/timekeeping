package com.iryna.spring.timekeeping.repository;

import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.entity.Progress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class ActivityRepositoryTest {

    @Autowired
    private ActivityRepository underTest;

    @AfterEach
    void tearDown(){
        underTest.deleteAll();
    }

    @Test
    void itShouldFindActivityById() {

        Long id = 1L;
        Activity activity = new Activity("Title", "MARKETING", Progress.TODO, "Some description");
        activity.setId(id);
        underTest.save(activity);

        Optional<Activity> expected = underTest.findActivityById(id);
        assertTrue(expected.isPresent());
        assertEquals(activity, expected.get());

    }

    @Test
    void itShouldNotFindActivityById() {
        Long id = 1L;
        Optional<Activity> expected = underTest.findActivityById(id);
        assertFalse(expected.isPresent());
    }

}