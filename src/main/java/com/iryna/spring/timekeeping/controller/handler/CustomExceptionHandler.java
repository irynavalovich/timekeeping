package com.iryna.spring.timekeeping.controller.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.NoSuchElementException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Log4j2
@ControllerAdvice()
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleSqlIntegrityException(HttpServletRequest request,
                                                              SQLIntegrityConstraintViolationException e){

        String message = "Unable to complete request " + e.getMessage();
        return buildResponseEntity(new ErrorResponse(HttpStatus.BAD_REQUEST, message));

    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Object> handleNoSuchElementException(HttpServletRequest request,
                                                              NoSuchElementException e){

        ErrorResponse response = new ErrorResponse(HttpStatus.NOT_FOUND);
        response.setMessage("Page is not existent: " + request.getRequestURI());
        return buildResponseEntity(response);

    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity handleDataIntegrityException(HttpServletRequest request,
                                                       DataIntegrityViolationException e){

        String message = "Unable to complete request " + e.getMessage() + " " + request.getRequestURI();
        return buildResponseEntity(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, message));

    }

    private ResponseEntity buildResponseEntity(ErrorResponse error){

        return new ResponseEntity(error, error.getStatus());

    }
}
