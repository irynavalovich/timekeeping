package com.iryna.spring.timekeeping.exception;

public class ActivityNotFoundException extends RuntimeException {

    public ActivityNotFoundException(String message) {

        super(message);

    }
}
