DELETE FROM activity_requests;

INSERT INTO activity_requests (id, action, status, activity_id, user_id) VALUES
    (1, 'Join', 'Pending Approval', 1, 1),
    (2, 'Exclude', 'Pending Approval', 1, 1);
