package com.iryna.spring.timekeeping.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SmokeTest {

    @Autowired private MainController controller1;
    @Autowired private UserController controller2;
    @Autowired private ActivityController controller3;
    @Autowired private RequestController controller4;
    @Autowired private UserUpdateController controller5;

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller1).isNotNull();
        assertThat(controller2).isNotNull();
        assertThat(controller3).isNotNull();
        assertThat(controller4).isNotNull();
        assertThat(controller5).isNotNull();
    }

}
