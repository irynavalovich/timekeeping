package com.iryna.spring.timekeeping.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;

/**
 * @description Data transfer object for Duration to transport object from client to business logic
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DurationDto implements Serializable {

    @PositiveOrZero(message = "{validation.dto.number.days}")
    private Integer days = 0;

    @PositiveOrZero(message = "{validation.dto.number.hours}")
    private Integer hours = 0;

    @PositiveOrZero(message = "{validation.dto.number.minutes}")
    private Integer minutes = 0;

}
