package com.iryna.spring.timekeeping.repository;

import com.iryna.spring.timekeeping.entity.ActivityRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *  JPA Repository provides access to ActivityRequest entity mapping in database.
 */
@Repository
public interface ActivityRequestRepository extends JpaRepository<ActivityRequest, Long> {

    /**
     * Search an activity request in the database by its identifier.
     *
     * @param id An activity request ID as a long value
     * @return Optional activity request
     */
    Optional<ActivityRequest> findActivityRequestById(Long id);

    /**
     * Search activity requests in the database by activity and user identifiers.
     *
     * @param activityId An activity ID as a long value
     * @param userId An user ID as a long value
     * @return List of activity requests
     */
    List<ActivityRequest> findByActivityIdAndUserId(Long activityId, Long userId);

}
