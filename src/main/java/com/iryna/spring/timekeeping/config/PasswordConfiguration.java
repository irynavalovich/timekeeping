package com.iryna.spring.timekeeping.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @description The class is responsible for encryption
 */

@Configuration
public class PasswordConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder(){

        return new BCryptPasswordEncoder(11);

    }

}
