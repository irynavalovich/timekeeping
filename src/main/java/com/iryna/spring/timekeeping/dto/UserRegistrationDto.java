package com.iryna.spring.timekeeping.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @description Data transfer object for User to transport object from RegistrationController to business logic
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationDto implements Serializable {

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 2, max = 100, message = "{user.registration.dto.firstname}")
    private String firstName;

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 2, max = 100, message = "{user.registration.dto.firstname}")
    private String lastName;

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 5, max = 40, message = "{user.registration.dto.username}")
    private String username;

    @NotBlank(message = "{validation.dto.not.blank}")
    @Size(min = 5, max = 40, message = "{user.registration.dto.username}")
    private String password;

    @Email(message = "{user.registration.dto.email}")
    private String email;

}
