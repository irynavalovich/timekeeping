package com.iryna.spring.timekeeping.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


class DurationDtoTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @ParameterizedTest
    @MethodSource("provideArgsForValidDuration")
    public void testValidDurationDto(DurationDto duration, boolean expected) {
        Set<ConstraintViolation<DurationDto>> violations = validator.validate(duration);

        assertEquals(expected, violations.isEmpty());

    }

    private static Stream<Arguments> provideArgsForValidDuration() {

        DurationDto d1 = new DurationDto(1, 1, 1);
        DurationDto d2 = new DurationDto(0, 1, 1);
        DurationDto d3 = new DurationDto(0, 1, 0);
        DurationDto d4 = new DurationDto(1, 0, 1);
        DurationDto d5 = new DurationDto(0, 5, 59);

        return Stream.of(
                Arguments.of(d1, true),
                Arguments.of(d2, true),
                Arguments.of(d3, true),
                Arguments.of(d4, true),
                Arguments.of(d5, true)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgsForInvalidDuration")
    public void testInvalidDurationDto(DurationDto duration, boolean expected) {
        Set<ConstraintViolation<DurationDto>> violations = validator.validate(duration);

        assertEquals(expected, violations.isEmpty());

    }

    private static Stream<Arguments> provideArgsForInvalidDuration() {

        DurationDto d1 = new DurationDto(-1, 1, 1);
        DurationDto d2 = new DurationDto(5, -6, 1);
        DurationDto d3 = new DurationDto(0, 1, -2);
        DurationDto d4 = new DurationDto(-1, -1, 1);


        return Stream.of(
                Arguments.of(d1, false),
                Arguments.of(d2, false),
                Arguments.of(d3, false),
                Arguments.of(d4, false)
        );
    }

}