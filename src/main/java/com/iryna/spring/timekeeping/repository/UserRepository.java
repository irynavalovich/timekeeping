package com.iryna.spring.timekeeping.repository;

import com.iryna.spring.timekeeping.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *  JPA Repository provides access to User entity mapping in database.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Search a user in the database by his identifier.
     *
     * @param id A user ID as a long value
     * @return Optional user
     */
    Optional<User> findUserById(Long id);

    /**
     * Search a user in the database by his username.
     *
     * @param username A String value
     * @return Optional user
     */
    Optional<User> findUserByUsername(String username);
}
