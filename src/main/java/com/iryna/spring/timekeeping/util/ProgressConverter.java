package com.iryna.spring.timekeeping.util;

import com.iryna.spring.timekeeping.entity.Progress;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * @description Convertion of Progress enum into String representation and vice versa correctly
 */

@Converter(autoApply = true)
public class ProgressConverter implements AttributeConverter<Progress, String> {

    @Override
    public String convertToDatabaseColumn(Progress attribute) {

        return (attribute == null) ? null : attribute.getValue();

    }

    @Override
    public Progress convertToEntityAttribute(String dbData) {

        return (dbData == null) ? null : Stream.of(Progress.values())
                .filter(category -> category.getValue().equals(dbData))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);

    }

}
