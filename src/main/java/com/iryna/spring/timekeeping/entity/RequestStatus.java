package com.iryna.spring.timekeeping.entity;

public enum RequestStatus {

    PENDING_APPROVAL("Pending Approval"),
    APPROVED("Approved"),
    REJECTED("Rejected");

    private final String value;

    RequestStatus(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }
}
