package com.iryna.spring.timekeeping.service.impl;

import com.iryna.spring.timekeeping.entity.Activity;
import com.iryna.spring.timekeeping.entity.ActivityRequest;
import com.iryna.spring.timekeeping.entity.Progress;
import com.iryna.spring.timekeeping.entity.RequestAction;
import com.iryna.spring.timekeeping.entity.RequestStatus;
import com.iryna.spring.timekeeping.entity.Role;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.ActivityRequestNotFoundException;
import com.iryna.spring.timekeeping.exception.UserNotFoundException;
import com.iryna.spring.timekeeping.repository.ActivityRepository;
import com.iryna.spring.timekeeping.repository.ActivityRequestRepository;
import com.iryna.spring.timekeeping.repository.UserRepository;
import com.iryna.spring.timekeeping.service.ActivityRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ActivityRequestServiceImplTest {

    @Mock
    private ActivityRepository activityRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ActivityRequestRepository activityRequestRepository;
    private ActivityRequestService underTest;
    @Captor
    ArgumentCaptor<ActivityRequest> requestArgumentCaptor = ArgumentCaptor.forClass(ActivityRequest.class);

    @BeforeEach
    void setUp() {
        underTest = new ActivityRequestServiceImpl(activityRequestRepository, userRepository, activityRepository);
    }

    @Test
    void whenFindActivityRequestByIdIsSuccesful() {

        Activity activity = new Activity();
        User user = new User();

        ActivityRequest expected = ActivityRequest.builder()
                .activity(activity)
                .user(user)
                .action(RequestAction.JOIN)
                .status(RequestStatus.PENDING_APPROVAL)
                .build();

        activityRequestRepository.save(expected);

        when(activityRequestRepository.findActivityRequestById(expected.getId()))
                .thenReturn(Optional.of(expected));

        ActivityRequest actual = underTest.findActivityRequestById(expected.getId());
        assertEquals(expected,actual);

        verify(activityRequestRepository, Mockito.times(1))
                .save(expected);
        verify(activityRequestRepository, Mockito.times(1))
                .findActivityRequestById(expected.getId());

    }

    @Test
    void throwExceptionWhenNotFindRequestById(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityRequestNotFoundException.class)
                .isThrownBy(() -> underTest.findActivityRequestById(id))
                .withMessage("Request by id" + id + "was not found");

        verify(activityRequestRepository, times(1)).findActivityRequestById(id);

    }

    @Test
    void throwExceptionWhenUserIdIsNotFoundInJoinActivityRequest(){

        Long userId = 5L;
        Long value = 1L;

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> underTest.createJoinActivityRequest(value, userId))
                .withMessage("User by id" + userId + "was not found");

        verify(userRepository, Mockito.times(1)).findUserById(userId);

    }


    @Test
    void throwExceptionWhenActivityRequestIsNotFoundSetApproveStatusForExclude(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityRequestNotFoundException.class)
                .isThrownBy(() -> underTest.setApproveStatusForExcludeActivityRequest(id))
                .withMessage("Request by id" + id + "was not found");

        verify(activityRequestRepository, times(1)).findActivityRequestById(id);

    }

    @Test
    void throwExceptionWhenActivityRequestIsNotFoundSetApproveStatusForJoin(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityRequestNotFoundException.class)
                .isThrownBy(() -> underTest.setApproveStatusForJoinActivityRequest(id))
                .withMessage("Request by id" + id + "was not found");

        verify(activityRequestRepository, times(1)).findActivityRequestById(id);

    }

    @Test
    void throwExceptionWhenActivityRequestIsNotFoundForRejectRequest(){

        Long id = 5L;

        assertThatExceptionOfType(ActivityRequestNotFoundException.class)
                .isThrownBy(() -> underTest.rejectActivityRequest(id))
                .withMessage("Request by id" + id + "was not found");

        verify(activityRequestRepository, times(1)).findActivityRequestById(id);

    }

    @Test
    void returnFalseWhenSetApproveStatusForExcludeActivityRequestTODO(){

        Activity a = new Activity("Title", "Category", Progress.TODO, "Description");
        a.setId(27L);

        User u = new User();

        ActivityRequest r =
                new ActivityRequest(10L, u, a, RequestStatus.PENDING_APPROVAL, RequestAction.EXCLUDE);

        activityRequestRepository.save(r);

        when(activityRequestRepository.findActivityRequestById(r.getId()))
        .thenReturn(Optional.of(r));

        assertFalse(underTest.setApproveStatusForExcludeActivityRequest(10L));
        verify(activityRequestRepository, times(1))
                .save(requestArgumentCaptor.capture());

    }

    @Test
    void returnFalseWhenSetApproveStatusForExcludeActivityRequestDISABLED() {
        Activity a = new Activity("Title", "Category", Progress.DISABLED, "Description");
        a.setId(27L);

        User u = User.builder()
                .username("username")
                .enabled(true)
                .role(Role.USER)
                .firstName("Timmy")
                .lastName("Hanks")
                .password("timmypassword")
                .id(27L)
                .build();

        ActivityRequest r =
                new ActivityRequest(10L, u, a, RequestStatus.PENDING_APPROVAL, RequestAction.EXCLUDE);

        activityRequestRepository.save(r);

        when(activityRequestRepository.findActivityRequestById(r.getId()))
                .thenReturn(Optional.of(r));

        assertFalse(underTest.setApproveStatusForExcludeActivityRequest(10L));
        assertEquals(RequestStatus.REJECTED, r.getStatus());
        verify(activityRequestRepository, times(2))
                .save(requestArgumentCaptor.capture());
    }

    @Test
    void setApproveStatusForExcludeActivityRequestSuccessful() {

        Activity a = new Activity("Title", "Category", Progress.ACTIVE, "Description");
        a.setId(27L);
        a.setUsersCount(1);
        User u = new User();

        ActivityRequest r =
                new ActivityRequest(10L, u, a, RequestStatus.PENDING_APPROVAL, RequestAction.EXCLUDE);

        activityRequestRepository.save(r);

        when(activityRequestRepository.findActivityRequestById(r.getId()))
                .thenReturn(Optional.of(r));

        assertTrue(underTest.setApproveStatusForExcludeActivityRequest(10L));
        assertEquals(Progress.DISABLED, r.getActivity().getProgress());
        assertEquals(RequestStatus.APPROVED, r.getStatus());
        verify(activityRequestRepository, times(2))
                .save(requestArgumentCaptor.capture());
        verify(activityRepository, times(1))
                .save(a);
    }

    @Test
    void returnTrueWhenSetApproveStatusForJoinActivityRequestTODO(){

        Activity a = new Activity("Title", "Category", Progress.TODO, "Description");
        a.setId(27L);
        a.setUsersCount(1);
        User u = User.builder()
                .id(27L)
                .password("userpassword")
                .firstName("Timmy")
                .lastName("Hanks")
                .role(Role.USER)
                .enabled(true)
                .email("timmy@mail.com")
                .username("username")
                .activities(new ArrayList<>())
                .build();

        ActivityRequest r =
                new ActivityRequest(10L, u, a, RequestStatus.PENDING_APPROVAL, RequestAction.JOIN);

        activityRequestRepository.save(r);

        when(activityRequestRepository.findActivityRequestById(r.getId()))
                .thenReturn(Optional.of(r));

        assertTrue(underTest.setApproveStatusForJoinActivityRequest(10L));
        assertEquals(Progress.ACTIVE, r.getActivity().getProgress());
        assertEquals(2, r.getActivity().getUsersCount());
        assertEquals(RequestStatus.APPROVED, r.getStatus());
        verify(activityRequestRepository, times(2))
                .save(requestArgumentCaptor.capture());
        verify(activityRepository, times(1))
                .save(a);

    }

    @Test
    void returnFalseWhenSetApproveStatusForJoinActivityRequestDisabled() {

        Activity a = new Activity("Title", "Category", Progress.DISABLED, "Description");
        a.setId(27L);
        a.setUsersCount(1);
        User u = User.builder()
                .id(27L)
                .password("userpassword")
                .firstName("Timmy")
                .lastName("Hanks")
                .role(Role.USER)
                .enabled(true)
                .email("timmy@mail.com")
                .username("username")
                .activities(new ArrayList<>())
                .build();
        ActivityRequest r =
                new ActivityRequest(10L, u, a, RequestStatus.PENDING_APPROVAL, RequestAction.JOIN);

        activityRequestRepository.save(r);

        when(activityRequestRepository.findActivityRequestById(r.getId()))
                .thenReturn(Optional.of(r));

        assertFalse(underTest.setApproveStatusForJoinActivityRequest(10L));
        assertEquals(1, r.getActivity().getUsersCount());
        assertEquals(RequestStatus.REJECTED, r.getStatus());
        verify(activityRequestRepository, times(2))
                .save(requestArgumentCaptor.capture());
        verify(activityRepository, times(0))
                .save(a);
    }

    @Test
    void returnTrueWhenSetApproveStatusForJoinActivityRequestOtherCases(){

        Activity a = new Activity("Title", "Category", Progress.ACTIVE, "Description");
        a.setId(27L);
        a.setUsersCount(1);

        User u = User.builder()
                .id(27L)
                .password("userpassword")
                .firstName("Timmy")
                .lastName("Hanks")
                .role(Role.USER)
                .enabled(true)
                .email("timmy@mail.com")
                .username("username")
                .activities(new ArrayList<>())
                .build();

        ActivityRequest r =
                new ActivityRequest(10L, u, a, RequestStatus.PENDING_APPROVAL, RequestAction.JOIN);

        activityRequestRepository.save(r);

        when(activityRequestRepository.findActivityRequestById(r.getId()))
                .thenReturn(Optional.of(r));

        assertTrue(underTest.setApproveStatusForJoinActivityRequest(10L));
        assertEquals(Progress.ACTIVE, r.getActivity().getProgress());
        assertEquals(2, r.getActivity().getUsersCount());
        assertEquals(RequestStatus.APPROVED, r.getStatus());
        verify(activityRequestRepository, times(2))
                .save(requestArgumentCaptor.capture());
        verify(activityRepository, times(1))
                .save(a);

    }
}