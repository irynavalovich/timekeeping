package com.iryna.spring.timekeeping.service.impl;

import com.iryna.spring.timekeeping.dto.UserUpdateDto;
import com.iryna.spring.timekeeping.entity.Role;
import com.iryna.spring.timekeeping.entity.User;
import com.iryna.spring.timekeeping.exception.UserNotFoundException;
import com.iryna.spring.timekeeping.repository.UserRepository;
import com.iryna.spring.timekeeping.service.UserService;
import com.iryna.spring.timekeeping.dto.UserRegistrationDto;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@AllArgsConstructor
@Log4j2
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Page<User> getAllPageable(Pageable pageable) {

        return this.userRepository.findAll(pageable);

    }

    @Override
    public boolean blockUnblockUser(Long id) {

        User user = userRepository.findUserById(id).orElseThrow(
                () -> new UserNotFoundException("User by id" + id + "was not found"));

        if (user.isEnabled()) {

            user.setEnabled(false);
            log.info("User with id" + id + " was blocked");
            userRepository.save(user);
            return true;

        }

        user.setEnabled(true);
        log.info("User with id" + id + " was unblocked");
        userRepository.save(user);
        return true;
    }

    @Override
    public User findUserById(Long id){

        return userRepository.findUserById(id).orElseThrow(
                () -> new UserNotFoundException("User by id" + id + "was not found")
        );

    }

    @Override
    public boolean createUser(UserRegistrationDto user) {

        if (userRepository.findUserByUsername(user.getUsername()).isPresent()) {
            log.info("User was not created");
            return false;
        }

        User newUser = User.builder().firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .password(passwordEncoder.encode(user.getPassword()))
                .email(user.getEmail())
                .role(Role.USER)
                .enabled(true).build();

        userRepository.save(newUser);
        log.info("User was created");

        return true;
    }

    @Override
    public void updateUser(Long id, UserUpdateDto user) {

        User newUser = userRepository.findUserById(id).orElseThrow(
                () -> new UserNotFoundException("User by id" + id + "was not found")
        );

        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setUsername(user.getUsername());

        if(Objects.nonNull(user.getPassword()) && user.getPassword().length() > 0){
            newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        newUser.setEmail(user.getEmail());
        newUser.setRole(user.getRole());
        userRepository.save(newUser);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return userRepository.findUserByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException("User by username " + username + " was not found")
        );

    }

}
