package com.iryna.spring.timekeeping.exception;

public class ActivityRequestNotFoundException extends RuntimeException {

    public ActivityRequestNotFoundException(String message) {

        super(message);

    }

}
