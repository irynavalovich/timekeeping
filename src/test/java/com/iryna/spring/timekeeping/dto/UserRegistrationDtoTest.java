package com.iryna.spring.timekeeping.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserRegistrationDtoTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testValidUserRegistrationDto() {
        UserRegistrationDto urdto =
                new UserRegistrationDto("Name", "Surname", "username", "password", "mail@mail.com");

        Set<ConstraintViolation<UserRegistrationDto>> violations = validator.validate(urdto);
        assertTrue(violations.isEmpty());

    }

    @ParameterizedTest
    @MethodSource("provideArgsForInvalidUser")
    void testInvalidUserRegistrationDto(UserRegistrationDto u, boolean expected) {

        Set<ConstraintViolation<UserRegistrationDto>> violations = validator.validate(u);

        assertThrows(IllegalArgumentException.class,
                () -> validator.validate(null));
        assertEquals(expected, violations.isEmpty());

    }

    private static Stream<Arguments> provideArgsForInvalidUser() {

        UserRegistrationDto u1 = new UserRegistrationDto("N", "Surname", "username", "password", "mail@mail.com");
        UserRegistrationDto u2 = new UserRegistrationDto("Name", "", "username", "password", "mail@mail.com");
        UserRegistrationDto u3 = new UserRegistrationDto("Name", "Surname", "", "password", "mail@mail.com");
        UserRegistrationDto u4 = new UserRegistrationDto("Name", "Surname", "username", "", "mail@mail.com");
        UserRegistrationDto u5 = new UserRegistrationDto("Name", "Surname", "username", "password", "mail@");

        return Stream.of(
                Arguments.of(u1, false),
                Arguments.of(u2, false),
                Arguments.of(u3, false),
                Arguments.of(u4, false),
                Arguments.of(u5, false)

        );
    }
}
