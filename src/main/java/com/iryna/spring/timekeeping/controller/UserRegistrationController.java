package com.iryna.spring.timekeeping.controller;

import com.iryna.spring.timekeeping.service.UserService;
import com.iryna.spring.timekeeping.dto.UserRegistrationDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller for user-registration
 */

@Controller
@RequestMapping("/registration")
@AllArgsConstructor
public class UserRegistrationController {

    private final UserService userService;

    @GetMapping
    public String showRegistrationForm(@ModelAttribute("user") UserRegistrationDto userRegistrationDto){

        return "registration";

    }

    @PostMapping
    public String register(@ModelAttribute("user")  @Valid UserRegistrationDto userRegistrationDto,
                           BindingResult bindingResult){


        if (bindingResult.hasErrors()) {
            return "registration";
        }

        if (userService.createUser(userRegistrationDto)){
            return "redirect:/login";
        }

        return "registration";

    }

    @ModelAttribute("user")
    public UserRegistrationDto getUserRegistrationDto(){

        return new UserRegistrationDto();

    }


}
