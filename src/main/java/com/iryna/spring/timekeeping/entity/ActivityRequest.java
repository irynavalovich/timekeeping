package com.iryna.spring.timekeeping.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "activity_requests")
public class ActivityRequest {

    @Id
    @SequenceGenerator(name = "activityRequestIdSeq",
            sequenceName = "activity_requests_id_seq",
            allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "activityRequestIdSeq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Activity activity;

    @Enumerated(value = EnumType.STRING)
    private RequestStatus status;

    @Enumerated(value = EnumType.STRING)
    private RequestAction action;

    @Override
    public String toString() {
        return getClass().getSimpleName() +
                "id=" + getId() +
                ", user=" + getUser() +
                ", activity=" + getActivity() +
                ", status=" + getStatus().getValue() +
                ", action=" + getAction().getValue();
    }
}
